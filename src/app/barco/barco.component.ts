import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Barco } from './../models/barco.model';

@Component({
  selector: 'app-barco',
  templateUrl: './barco.component.html',
  styleUrls: ['./barco.component.css']
})
export class BarcoComponent implements OnInit {
  @Input() barco: Barco;
  @HostBinding('attr.class') cssClass='col-md-4';

  constructor() { }

  ngOnInit(): void {
  }

}
