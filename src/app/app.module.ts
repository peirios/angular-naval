import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BarcoComponent } from './barco/barco.component';
import { ListaBarcoComponent } from './lista-barco/lista-barco.component';

@NgModule({
  declarations: [
    AppComponent,
    BarcoComponent,
    ListaBarcoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
