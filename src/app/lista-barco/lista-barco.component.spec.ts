import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaBarcoComponent } from './lista-barco.component';

describe('ListaBarcoComponent', () => {
  let component: ListaBarcoComponent;
  let fixture: ComponentFixture<ListaBarcoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaBarcoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaBarcoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
