import { Component, OnInit } from '@angular/core';
import { Barco } from './../models/barco.model';

@Component({
  selector: 'app-lista-barco',
  templateUrl: './lista-barco.component.html',
  styleUrls: ['./lista-barco.component.css']
})
export class ListaBarcoComponent implements OnInit {
  barcos: Barco[];
  
  constructor() { 
    this.barcos = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, isbn:string, bandera:string):boolean {
    this.barcos.push(new Barco(nombre, isbn, bandera));
    return false;
  }

}
